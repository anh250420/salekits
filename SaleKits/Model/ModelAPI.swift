//
//  ModelAPI.swift
//  SaleKits
//
//  Created by devsenior1 on 06/05/2022.
//

import Foundation

struct RootClass: Codable {

    let title: String
    let detail: String
    let path: String
    let message: String

    private enum CodingKeys: String, CodingKey {
        case title = "title"
        case detail = "detail"
        case path = "path"
        case message = "message"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        title = try values.decode(String.self, forKey: .title)
        detail = try values.decode(String.self, forKey: .detail)
        path = try values.decode(String.self, forKey: .path)
        message = try values.decode(String.self, forKey: .message)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(title, forKey: .title)
        try container.encode(detail, forKey: .detail)
        try container.encode(path, forKey: .path)
        try container.encode(message, forKey: .message)
    }

}



