//
//  Child2ViewController.swift
//  SaleKits
//
//  Created by devsenior1 on 09/05/2022.
//

import UIKit
import XLPagerTabStrip

class Child2ViewController: UIViewController, IndicatorInfoProvider, UITableViewDelegate, UITableViewDataSource{
    @IBOutlet weak var Child2TB: UITableView!
    public var listdata2 = Cell.createRecipes()
    var childNumber: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        Child2TB.register(UINib(nibName: "QASanTableViewCell", bundle: nil), forCellReuseIdentifier: "QASanTableViewCell")
        Child2TB.dataSource = self
        Child2TB.delegate = self
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "\(childNumber)")
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.listdata2.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Child2TB.dequeueReusableCell(withIdentifier:"QASanTableViewCell", for: indexPath) as! QASanTableViewCell
        cell.NumBerLB.text = "\(self.listdata2[indexPath.row].Number)"
        return cell
    }
    @IBAction func NextPopup(_ sender: Any) {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "PoPupViewController") else { return }
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            present(vc, animated: true)
    }
}
