//
//  ViewController.swift
//  SaleKits
//
//  Created by devsenior1 on 06/05/2022.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var KHDNBtn: UIButton!
    @IBOutlet weak var KHCNBtn: UIButton!
    @IBOutlet weak var viewKHCN: UIView!
    @IBOutlet weak var viewDN: UIView!
    @IBOutlet weak var viewconDN: UIView!
    @IBOutlet weak var viewMK: UIView!
    @IBOutlet weak var viewconMK: UIView!
    @IBOutlet weak var usernameTxt: UITextField!
    @IBOutlet weak var PassWordTxt: UITextField!
    @IBOutlet weak var DangNhapBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        viewKHCN.layer.cornerRadius = 10
        viewDN.layer.cornerRadius = 10
        viewconDN.layer.cornerRadius = 10
        viewMK.layer.cornerRadius = 10
        viewconMK.layer.cornerRadius = 10
        DangNhapBtn.layer.cornerRadius = 10
    }
   
    @IBAction func KHCNphai(_ sender: Any) {
        KHCNBtn.setImage(UIImage(named: "KHCN.png"), for: .normal)
        KHDNBtn.setImage(UIImage(named: "KHDN.png"), for: .normal)
    }
    
    @IBAction func KHCNtrai(_ sender: Any) {
        KHCNBtn.setImage(UIImage(named: "KHCND.png"), for: .normal)
        KHDNBtn.setImage(UIImage(named: "KHDND.png"), for: .normal)
    }
    @IBAction func DangNhapBtn(_ sender: Any) {
        if usernameTxt.text == "" || PassWordTxt.text == "" {
            let alert = UIAlertController(title: "Thông báo", message: "Thông tin đăng nhập không chính xác.", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Đóng", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
        }else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Man2ViewController") as! Man2ViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
//    func getData(from url:String) {
//               let task = URLSession.shared.dataTask(with: URL(string: url)!, completionHandler: { data, response, error in
//                    guard let data = data, error == nil else {
//                        print("somwthing went wrong")
//                        return
//                    }
//
//                    var result: RootClass?
//                    do {
//                    result = try JSONDecoder().decode(RootClass.self, from: data)
//                    }
//                    catch{
//                        print("failed to convert \(error)")
//                    }
//                    guard let json = result else {
//                        return
//                            print("111111 \(error)")
//                    }
//
//               })
//               task.resume()
//            }
    
  


