//
//  PoPupViewController.swift
//  SaleKits
//
//  Created by devsenior1 on 09/05/2022.
//

import UIKit
import iOSDropDown

class PoPupViewController: UIViewController {

    @IBOutlet weak var DropDown1: DropDown!
    @IBOutlet weak var DropDown2: DropDown!
    @IBOutlet weak var DropDown3: DropDown!
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let option =  Options()
        DropDown1.optionArray = option.countries
        DropDown1.optionIds = option.ids
        DropDown1.checkMarkEnabled = false
        DropDown1.arrowSize = 10
        DropDown2.optionArray = option.color
        DropDown2.optionIds = option.ids
        DropDown2.checkMarkEnabled = false
        DropDown2.arrowSize = 10
        DropDown3.optionArray = option.boolData
        DropDown3.optionIds = option.ids
        DropDown3.checkMarkEnabled = false
        DropDown3.arrowSize = 10
    }
    

 
    @IBAction func Close(_ sender: Any) {
        dismiss(animated: true)
    }
}
