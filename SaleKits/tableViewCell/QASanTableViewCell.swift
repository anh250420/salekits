//
//  QASanTableViewCell.swift
//  SaleKits
//
//  Created by devsenior1 on 08/05/2022.
//

import UIKit

class QASanTableViewCell: UITableViewCell {

    @IBOutlet weak var ViewNgoai: UIView!
    @IBOutlet weak var NumBerLB: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        ViewNgoai.layer.cornerRadius = 10
        ViewNgoai.layer.shadowOpacity = 0.4
        ViewNgoai.layer.shadowColor = UIColor.gray.cgColor
        ViewNgoai.layer.shadowOffset = CGSize(width: 0, height: 0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
