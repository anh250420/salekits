//
//  ChildViewController.swift
//  SaleKits
//
//  Created by devsenior1 on 06/05/2022.
//

import UIKit
import XLPagerTabStrip

class ChildViewController: UIViewController, IndicatorInfoProvider, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var ChildTB: UITableView!
    public var listdata = Cell.createRecipes()
    var childNumber: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ChildTB.register(UINib(nibName: "QASanTableViewCell", bundle: nil), forCellReuseIdentifier: "QASanTableViewCell")
        ChildTB.dataSource = self
        ChildTB.delegate = self
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.listdata.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ChildTB.dequeueReusableCell(withIdentifier:"QASanTableViewCell", for: indexPath) as! QASanTableViewCell
        cell.NumBerLB.text = "\(self.listdata[indexPath.row].Number)"
        return cell
    }
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "\(childNumber)")
    }

}
